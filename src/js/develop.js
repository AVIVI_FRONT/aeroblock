function sliderInit() {
    var slider01 = $('.js-slick-fade');
    if(slider01.length > 0){
        slider01.slick({
            fade:true,
            dots: true,
            arrows:true,
            prevArrow:'<button type="button" class="slick-prev"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            nextArrow:'<button type="button" class="slick-next"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            infinite: true,
            speed: 300,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 1,
            adaptiveHeight: true
        });
    }
    var slider02 = $('.js-slick-fade-another');
    if(slider02.length > 0){
        slider02.slick({
            fade:true,
            dots: false,
            arrows:true,
            prevArrow:'<button type="button" class="slick-prev"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            nextArrow:'<button type="button" class="slick-next"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true
        });
    }
    var slider03 = $('.js-rows-slider');
    if(slider03.length > 0){
        slider03.slick({
            dots: false,
            infinite: false,
            speed: 300,
            rows: 2,
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 2,
                        dots: true
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 1,
                        dots: true
                    }
                }
                ]
        });
    }
    var slider04 = $('.js-three-items-slider');
    if(slider04.length > 0){
        slider04.slick({
            dots: true,
            infinite: true,
            speed: 300,
            prevArrow:'<button type="button" class="slick-prev"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            nextArrow:'<button type="button" class="slick-next"><svg width="43" height="100"><use xlink:href="#arrow"></use></svg></button>',
            slidesToShow: 3,
            responsive: [
                { breakpoint: 1000, settings: { slidesToShow: 2 } },
                { breakpoint: 700, settings: { slidesToShow: 1, fade:true } }
            ]
        });
    }
    var slider03 = $('.js-partner-slider');
    if(slider03.length > 0){
        slider03.slick({
            dots: false,
            infinite: false,
            speed: 300,
            rows: 2,
            slidesToShow: 5,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {  slidesToShow: 4}
                },
                {
                    breakpoint: 1000,
                    settings: {  slidesToShow: 3}
                },
                {
                    breakpoint: 600,
                    settings: {  rows: 3,slidesToShow: 2 }
                }
            ]
        });
    }
}
function stickyHeader(){
    var h = $('header');
    var m = $('.main');
    if(h.length > 0) {
        $(window).on('scroll', function () {
            if(window.pageYOffset > 66){
                h.addClass('fixed');
                m.addClass('ppth');
            }else{
                h.removeClass('fixed');
                m.removeClass('ppth');
            }
        })
    }
}
function butter() {
    var div = $('.header__mobile');
    var menuMobile = $('.js-open-menu');
    var close = $('.js-close-menu');
    menuMobile.on('click',function () {
      div.stop().slideDown();
    });
    close.on('click',function () {
        div.stop().slideUp();
    });
    $(document).on('click touchstart',function (event){
        if (!div.is(event.target) && div.has(event.target).length === 0 && !menuMobile.is(event.target) && menuMobile.has(event.target).length === 0)  {  div.stop().slideUp();  }
    });
}
function bestMapInit() {
    var mapWrap = 'map';
    if ($('#map').length == 0) return false;
    var myLatlng = new google.maps.LatLng(cordX,cordY);
    var myOptions = {
        zoom: 10,
        center: myLatlng,
        disableDefaultUI: false, //без управляющих елементов
        mapTypeId: google.maps.MapTypeId.ROADMAP, // SATELLITE - снимки со спутника,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM // позиция слева внизу для упр елементов
        }
    }
    var map = new google.maps.Map(document.getElementById(mapWrap), myOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        animation: google.maps.Animation.DROP, // анимация при загрузке карты

    });

    /*анимация при клике на маркер*/
    marker.addListener('click', toggleBounce);
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    /*/анимация при клике на маркер*/

    /*По клику открываеться инфоблок*/
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });

}

$(document).ready(function () {
    butter();
    // bestMapInit();
    sliderInit();
    stickyHeader();


});